﻿//Global include
#include <iostream>
#include <exception>
#include <filesystem>
#include <fstream>
#include <algorithm>
#include <random>
#include <chrono>
#include <vector>
#include <string>
#include <stack>
// Local include
#include "graph/Graph.hpp"
#include "Algorithm-Dijkstra.h"

namespace fs = std::filesystem;

using std::vector;
using std::string;

struct exceptionCase {
	bool exception;
	string exceptionMassage;

	exceptionCase() : exception(false), exceptionMassage("") {}
	exceptionCase(const bool _exception, const string& _exceptionMassage ) : exception(_exception), exceptionMassage(_exceptionMassage) {}
	exceptionCase(const exceptionCase& _exceptionCase): exception(_exceptionCase.exception), exceptionMassage(_exceptionCase.exceptionMassage) {}
	
	bool isExcepted() const {
		return exception;
	}
	const string& getException() const {
		return exceptionMassage;
	}
};
struct bracketsCase {
	bool correctBrackets;
	string stringOfBrackets;

	bracketsCase() : correctBrackets(true), stringOfBrackets("") {}
	bracketsCase(const bool _correctBrackets, const string& _stringOfBrackets) : correctBrackets(_correctBrackets), stringOfBrackets(_stringOfBrackets) {}
	bracketsCase(const bracketsCase& _bracketsCase) : correctBrackets(_bracketsCase.correctBrackets), stringOfBrackets(_bracketsCase.stringOfBrackets) {}

	bool isCorrectBrackets() const {
		return correctBrackets;
	}
	const string& getException() const {
		return stringOfBrackets;
	}
};
struct checkResponse {
	exceptionCase exCase;
	bracketsCase brCase;

	checkResponse() : exCase(), brCase() {}
	checkResponse(const exceptionCase& _exceptionCase, const bracketsCase& _bracketsCase) : exCase(_exceptionCase), brCase(_bracketsCase) {}
	checkResponse(const checkResponse& _checkResponse) : exCase(_checkResponse.exCase), brCase(_checkResponse.brCase) {}

	const exceptionCase& getExCase() const {
		return exCase;
	}
	const bracketsCase& getBrCase() const {
		return brCase;
	}
};

exceptionCase checkTextForChar(const string& text) {
	bool isExcepted = false;
	string exceptionMassage = "";

	if (text.length() < 13) {
		bool isExcepted = true;
		exceptionMassage = "Недостаточно окрестностей вершин!";
	}
	else {
		for (std::string::size_type i = 0; i < text.length(); ++i) {
			if (text[i] >= '0' && text[i] <= '9' || text[i] == '{' || text[i] == '}' || text[i] == ',') {
				continue;
			} else {
				bool isExcepted = true;
				exceptionMassage = "Недопустимые символы в файле!";
			}		 
		}
	}

	return exceptionCase(isExcepted,exceptionMassage);
}
bracketsCase checkTextForBrackets(const string& text) {
	bool correctBrackets = true;
    string bracketsResult = "";

	std::stack<char> stackOfBrackets;

	for (std::string::size_type i = 0; i < text.length(); ++i) {

		if (text[i] == '{') {
			stackOfBrackets.push('{');
			bracketsResult += '{';
		}
		else if (text[i] == '}') {
			if (stackOfBrackets.empty()) {
				correctBrackets = false;
				bracketsResult += " ->{";
				bracketsResult += '}';
			}
			else {
				stackOfBrackets.pop();
				bracketsResult += '}';
			}
		}
	}

	if (!stackOfBrackets.empty()) {
		correctBrackets = false;
		while (!stackOfBrackets.empty()) {
			stackOfBrackets.pop();
			bracketsResult += " ->}";
		}
	}

	return bracketsCase(correctBrackets,bracketsResult);
}
checkResponse multiCheckText(const string& text) {
	exceptionCase exCase = checkTextForChar(text);
	bracketsCase brCase = checkTextForBrackets(text);

	return checkResponse(exCase, brCase);
}

void fileCheckText(const char* url) {
    std::ifstream file(url);
    if (!file.is_open()) 
        throw std::exception("Файл не может быть открыт! (Проверьте веденный путь к файлу)");
    
	int counterLine = 0;
    string line;

	while (std::getline(file, line)) {
		if (line != "") {
			++counterLine;
			checkResponse chResponse = multiCheckText(line);
			if (chResponse.exCase.exception) {
				string massage(chResponse.exCase.exceptionMassage.data());
				massage += "На " + std::to_string(counterLine) + " значющей строке.";
				throw std::exception(massage.data());
			}
			if (!chResponse.brCase.correctBrackets) {
				string massage(chResponse.brCase.stringOfBrackets.data());
				massage += "На " + std::to_string(counterLine) + " значющей строке.";
				throw std::exception(chResponse.brCase.stringOfBrackets.data());
			}
		}
	}

	if (counterLine % 2 != 0) 
		throw std::exception("Размер матриц имен вершин и весов не совпадают!");
	
	file.close();
}

vector<vector<int>> formVector(string line) {
	line = line.substr(1, line.size() - 2);

	vector<vector<int>> vectorOfVector;
	vector<int> vectorOfValue;

	int value = 0;
	int rank = 1;

	for (auto it = line.begin(); it != line.end(); ++it) {
		if (*it == '{') {
			vectorOfValue.clear();
		}
		else if (*it == '}') {
			vectorOfValue.push_back(value);
			vectorOfVector.push_back(vectorOfValue);
		}
		else {
			if (*it >= '0' && *it <= '9') {
				value *= rank;
				value += *it - '0';
				rank = 10;
			}
			else {
				vectorOfValue.push_back(value);
				value = 0;
				rank = 1;
			}
		}
	}

	return vectorOfVector;
}
void compareVectorOfVector(const vector<vector<int>>& fVectorOfVector, const vector<vector<int>>& sVectorOfVector, const int numberfVectorOfVector) {

	if (fVectorOfVector.size() != sVectorOfVector.size()) {
		string massage("Размерность матриц не совпадает, ");
		massage += std::to_string(fVectorOfVector.size()) + " != " + std::to_string(sVectorOfVector.size()) + ". ";
		massage += "На строке " + std::to_string(numberfVectorOfVector) + " и " + std::to_string(numberfVectorOfVector + 1) + ".";
		throw std::exception(massage.data());
	}

	for (size_t i = 0; i < fVectorOfVector.size(); ++i) {
		vector<int> fVector = fVectorOfVector[i];
		vector<int> sVector = sVectorOfVector[i];

		if (fVector.size() != sVector.size()) {
			string massage("Размерность матриц не совпадает, ");
			massage += std::to_string(fVector.size()) + " != " + std::to_string(sVector.size()) + ". ";
			massage += "На строке " + std::to_string(numberfVectorOfVector) + " и " + std::to_string(numberfVectorOfVector + 1) + ". ";
			massage += "В " + std::to_string(i) + " записи.";
			throw std::exception(massage.data());
		}
	}
}

void formGraph(vector<Graph<int, int>>* vectorOfGraph, const char* url) {
	fileCheckText(url);
	std::ifstream file(url);

	string line;
	bool isFirstVector = true;
	int numberFVector = 0;

	vector<vector<int>> vectorOfName, vectorOfWeight;

	while (std::getline(file, line)) {
		if (line != "") {
			if (isFirstVector) {
				isFirstVector = false;
				++numberFVector;
				vectorOfName = formVector(line);
			}
			else {
				isFirstVector = true;
				vectorOfWeight = formVector(line);

				compareVectorOfVector(vectorOfName, vectorOfWeight, numberFVector);
				vectorOfGraph->push_back(Graph<int, int>(vectorOfName, vectorOfWeight));
			}
		}
		else {
			++numberFVector;
		}
	}

	file.close();
}

void generateRandomVector(vector<int>* vectorOfValue, vector<int>::size_type vectorSize) {
	std::random_device dev;
	std::mt19937 gen(dev());

	vectorOfValue->resize(vectorSize);
	for (vector<int>::size_type i = 0; i < vectorSize; ++i) {
		vectorOfValue->at(i) = gen() % 1000;
	}
	
}
void generateConnectedGraph(vector<Graph<int, int>>* vectorOfGraph, vector<int>::size_type numbersOfVertex) {
	vector<vector<int>> vectorOfName(numbersOfVertex), vectorOfWeight(numbersOfVertex);
	vector<int> randomValue;
	generateRandomVector(&randomValue, numbersOfVertex);

	for (vector<int>::size_type i = 0; i < numbersOfVertex; ++i) {
		vectorOfName[i] = vector<int>();
		vectorOfWeight[i] = vector<int>();
		for (vector<int>::size_type j = 0; j < numbersOfVertex; ++j) {
			if (i != j) {
				vectorOfName[i].push_back(static_cast<int>(j));
				vectorOfWeight[i].push_back(randomValue[j]);
			}
		}
	}

	vectorOfGraph->push_back(Graph<int, int>(vectorOfName, vectorOfWeight));
}

void informationAlgorithmDijkstraOnDHeap(const vector<Graph<int, int>>& vectorOfGraph) {
	fs::create_directories("logout");

	std::ofstream outfile;
	outfile.open("logout/algorithmDijkstraOnDHeapLogout.txt", std::ofstream::out | std::ofstream::trunc);
	outfile << "Алгоритм Дейкстры на 3-куче." << std::endl << std::endl;

	for (size_t i = 0; i < vectorOfGraph.size(); ++i) {
		size_t currentLink = 0;
		vector<int> dist;
		vector<int> up;

		auto t1 = std::chrono::high_resolution_clock::now();
		algorithmDijkstraOnDHeap(vectorOfGraph[i], &dist, &up, vectorOfGraph[i].size(), currentLink);
		auto t2 = std::chrono::high_resolution_clock::now();

		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		outfile << "Кратчейшие пути для графа # " << i << std::endl;
		for (size_t j = 0; j < dist.size(); ++j) {
			outfile << "  От вершины " << currentLink << " до вершины " << j << " - " << dist[j] << ". Предпоследняя вершина на пути " << up[j] << "." << std::endl;
		}
		outfile << "Выполнение заняло - " << duration << " мс.";
		outfile << std::endl << std::endl;;
	}

	outfile.close();
}
void informationAlgorithmDijkstraOnMark(const vector<Graph<int, int>>& vectorOfGraph) {
	fs::create_directories("logout");

	std::ofstream outfile;
	outfile.open("logout/algorithmDijkstraOnMarkLogout.txt", std::ofstream::out | std::ofstream::trunc);
	outfile << "Алгоритм Дейкстры на метках." << std::endl << std::endl;

	for (size_t i = 0; i < vectorOfGraph.size(); ++i) {
		size_t currentLink = 0;
		vector<int> dist;
		vector<int> up;

		auto t1 = std::chrono::high_resolution_clock::now();
		algorithmDijkstraOnMark(vectorOfGraph[i], &dist, &up, currentLink);
		auto t2 = std::chrono::high_resolution_clock::now();

		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

		outfile << "Кратчейшие пути для графа # " << i << std::endl;
		for (size_t j = 0; j < dist.size(); ++j) {
			outfile << "  От вершины "<< currentLink << " до вершины " << j << " - " << dist[j] << ". Предпоследняя вершина на пути " << up[j] << "." << std::endl;
		}
		outfile << "Выполнение заняло - " << duration << " мс.";
		outfile << std::endl << std::endl;;
	}

	outfile.close();
}

int main() {
	setlocale(LC_ALL, "RU");

	try {
		string url;
		std::cout << "Введите путь к файлу (.txt) : ";
		std::cin >> url;

		vector<Graph<int, int>> graph;
		formGraph(&graph, url.data());
		generateConnectedGraph(&graph, 100);
		
		informationAlgorithmDijkstraOnDHeap(graph);
		informationAlgorithmDijkstraOnMark(graph);

		std::cout << std::endl << "Файлы результатов работы алгоритмов успешно сгенерировались!" << std::endl;
	}
	catch (std::exception e) {
		std::cout << e.what();
	}
	
    return 0;
}
