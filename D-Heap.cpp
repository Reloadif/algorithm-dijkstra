#include "D-Heap.h"

size_t fatherLink(const size_t amountOfElementHeap, const size_t link) {
    size_t fatherLink = 1000000;
    if (link != 0) {
        fatherLink = (link - 1) / 3;
    }

    return fatherLink;
}

size_t firstChildLink(const size_t amountOfElementHeap, const size_t link) {
    size_t firstChild = link * 3 + 1;
    firstChild = (firstChild >= amountOfElementHeap) ? 0 : firstChild;

    return firstChild;
}
size_t lastChildLink(const size_t amountOfElementHeap, const size_t link) {
    size_t lastChild = firstChildLink(amountOfElementHeap, link);

    if (lastChild != 0) {
        size_t expectedLastChild = lastChild + 2;

        while (lastChild < (amountOfElementHeap - 1) && lastChild < expectedLastChild) {
            lastChild += 1;
        }

    }

    return lastChild;
}

size_t smallestChildLink(const vector<int>& vectorOfKey, const size_t amountOfElementHeap, const size_t link) {
    size_t smallestChild = link;
    size_t firstChild = firstChildLink(amountOfElementHeap, link);

    if (firstChild != 0) {
        size_t lastChild = lastChildLink(amountOfElementHeap, link);

        smallestChild = firstChild;
        int smallestKey = vectorOfKey[smallestChild];

        for (size_t i = firstChild + 1; i <= lastChild; ++i) {
            if (smallestKey > vectorOfKey[i]) {
                smallestChild = i;
                smallestKey = vectorOfKey[i];
            }
        }
    }

    return smallestChild;
}

void immersion(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap, const size_t link) {
    if (vectorOfName.size() == vectorOfKey.size()) {
        size_t currentLink = link;
        size_t currentSmallestChild = smallestChildLink(vectorOfKey, amountOfElementHeap, currentLink);

        while ((currentSmallestChild != currentLink) && (vectorOfKey[currentLink] > vectorOfKey[currentSmallestChild])) {
            std::swap(vectorOfName[currentLink], vectorOfName[currentSmallestChild]);
            std::swap(vectorOfKey[currentLink], vectorOfKey[currentSmallestChild]);
            std::swap(vectorOfIndex[vectorOfName[currentLink]], vectorOfIndex[vectorOfName[currentSmallestChild]]);

            currentLink = currentSmallestChild;
            currentSmallestChild = smallestChildLink(vectorOfKey, amountOfElementHeap, currentLink);
        }
    } else {
        throw std::exception("������ ������� ���� � ������� ������ �� �����!");
    }
}
void surfacing(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap, const size_t link) {
    if (vectorOfName.size() == vectorOfKey.size()) {
        size_t currentLink = link;
        size_t currentFatherLink = fatherLink(amountOfElementHeap, currentLink);

        while ((currentFatherLink != 1000000) && (vectorOfKey[currentFatherLink] > vectorOfKey[currentLink])) {
            std::swap(vectorOfName[currentLink], vectorOfName[currentFatherLink]);
            std::swap(vectorOfKey[currentLink], vectorOfKey[currentFatherLink]);
            std::swap(vectorOfIndex[vectorOfName[currentLink]], vectorOfIndex[vectorOfName[currentFatherLink]]);

            currentLink = currentFatherLink;
            currentFatherLink = fatherLink(amountOfElementHeap, currentLink);
        }
    } else {
        throw std::exception("������ ������� ���� � ������� ������ �� �����!");
    }
}

void withdrawalOfTheMinimum(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, size_t& amountOfElementHeap, int& keyMinimum, int& nameMinimum) {
    if (vectorOfName.size() == vectorOfKey.size()) {
        nameMinimum = vectorOfName[0];
        keyMinimum = vectorOfKey[0];

        std::swap(vectorOfName[0], vectorOfName[amountOfElementHeap - 1]);
        std::swap(vectorOfKey[0], vectorOfKey[amountOfElementHeap - 1]);
        std::swap(vectorOfIndex[vectorOfName[0]], vectorOfIndex[vectorOfName[amountOfElementHeap - 1]]);

        amountOfElementHeap = amountOfElementHeap - 1;

        if (amountOfElementHeap > 1) {
            immersion(vectorOfName, vectorOfKey, vectorOfIndex, amountOfElementHeap, 0);
        }
    } else {
        throw std::exception("������ ������� ���� � ������� ������ �� �����!");
    }
}

void queueUp(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap) {
    if (vectorOfKey.size() == vectorOfName.size()) {
        for (int i = amountOfElementHeap - 1; i >= 0; --i) {
            immersion(vectorOfName, vectorOfKey, vectorOfIndex, amountOfElementHeap, i);
        }
    } else {
        throw std::exception("������ ������� ���� � ������� ������ �� �����!");
    }
}