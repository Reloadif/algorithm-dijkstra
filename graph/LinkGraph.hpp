#pragma once

template<typename nameType, typename weightType>
class Graph;

template<typename nameType, typename weightType>
class LinkGraph {
	nameType name;
	weightType weight;

	LinkGraph* nextLink;
public:
	LinkGraph();
	LinkGraph(const nameType _name, const weightType _weight);
	LinkGraph(const LinkGraph& _linkGraph);

	~LinkGraph();

	nameType getName() const;
	void setName(const nameType _name);

	weightType getWeight() const;
	void setWeight(const weightType _weight);

	LinkGraph* getNextLink() const;
	void setNextLink(LinkGraph* _nextLink);

	LinkGraph& operator= (const LinkGraph& _rightLinkGraph);

	friend class Graph<nameType, weightType>;
};

template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>::LinkGraph() : name(static_cast<nameType>(0)), weight(static_cast <weightType>(0)), nextLink(nullptr) {}
template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>::LinkGraph(const nameType _name, const weightType _weight) : name(_name), weight(_weight), nextLink(nullptr) {}
template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>::LinkGraph(const LinkGraph& _linkGraph) : name(_linkGraph.name), weight(_linkGraph.weight), nextLink(nullptr) {
	LinkGraph* currentlink = this->nextLink;
	LinkGraph* link = _linkGraph.nextLink;
	while (link != nullptr) {
		currentlink = new Graph(link->name, link->weight);
		currentlink = currentlink->nextLink;
		link = link->nextLink;
	}
}

template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>::~LinkGraph() {}

template<typename nameType, typename weightType>
nameType LinkGraph<nameType, weightType>::getName() const {
	return name;
}
template<typename nameType, typename weightType>
void LinkGraph<nameType, weightType>::setName(nameType _name) {
	if (name != _name) 
		name = _name;
}

template<typename nameType, typename weightType>
weightType LinkGraph<nameType, weightType>::getWeight() const {
	return weight;
}
template<typename nameType, typename weightType>
void LinkGraph<nameType, weightType>::setWeight(const weightType _weight) {
	if (weight != _weight)
		weight = _weight;
}

template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>* LinkGraph<nameType, weightType>::getNextLink() const {
	return nextLink;
}
template<typename nameType, typename weightType>
void LinkGraph<nameType, weightType>::setNextLink(LinkGraph* _nextLink) {
	if (nextLink != _nextLink)
		nextLink = _nextLink;
}

template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>& LinkGraph<nameType, weightType>::operator= (const LinkGraph& _rightLinkGraph) {
	if (this != &_rightLinkGraph) {
		name = _rightLinkGraph.name;
		weight = _rightLinkGraph.weight;

		LinkGraph* currentlink = this->nextLink;
		LinkGraph* link = _rightLinkGraph.nextLink;
		while (link != nullptr) {
			currentlink = new Graph(link->name, link->weight);
			currentlink = currentlink->nextLink;
			link = link->nextLink;
		}
	}

	return *this;
}
