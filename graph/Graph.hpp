#pragma once

// Global include
#include <vector>
// Local include
#include "LinkGraph.hpp"
#include "StreamGraph.hpp"

using std::vector;

template<typename nameType, typename weightType>
class Graph {
	size_t amountOfAllocatedMemory;

	LinkGraph<nameType, weightType>** arrayOfLinks;

	void destroy(LinkGraph<nameType, weightType>* _linkGraph);
	friend std::ostream& operator << <nameType, weightType> (std::ostream& stream, const Graph<nameType, weightType>& obj);
public:
	Graph();
	Graph(const vector<vector<nameType>> _vectorOfName, const vector<vector<weightType>> _vectorOfWeight);
	Graph(const Graph& _graph);

	~Graph();

	size_t size() const;
	LinkGraph<nameType, weightType>** data();

	LinkGraph<nameType, weightType>* getLinkGraph(const size_t _indexLink) const;

	Graph& operator= (const Graph& _rightGraph);
};

template<typename nameType, typename weightType>
void Graph<nameType, weightType>::destroy(LinkGraph<nameType, weightType>* _linkGraph) {
	vector<LinkGraph<nameType, weightType>*> linkQueue;

	while (_linkGraph != nullptr) {
		linkQueue.push_back(_linkGraph);
		_linkGraph = _linkGraph->nextLink;
	}

	for (int i = linkQueue.size() - 1; i >= 0; --i) {
		LinkGraph<nameType, weightType>* deleteLink = linkQueue[i];
		delete deleteLink;
	}
}

template<typename nameType, typename weightType>
Graph<nameType, weightType>::Graph() : amountOfAllocatedMemory(0), arrayOfLinks(nullptr) {}
template<typename nameType, typename weightType>
Graph<nameType, weightType>::Graph(const vector<vector<nameType>> _vectorOfName, const vector<vector<weightType>> _vectorOfWeight) {
	amountOfAllocatedMemory = _vectorOfName.size();
	arrayOfLinks = new LinkGraph<nameType, weightType>*[amountOfAllocatedMemory];

	for (size_t row = 0; row < _vectorOfName.size(); ++row) {
		for (size_t column = 0; column < _vectorOfName[row].size(); ++column) {
			if (column == 0) {
				arrayOfLinks[row] = new LinkGraph<nameType, weightType>(_vectorOfName[row][column], _vectorOfWeight[row][column]);
			} else {
				LinkGraph<nameType, weightType>* currentLink = arrayOfLinks[row];
				while (currentLink->nextLink != nullptr) {
					currentLink = currentLink->nextLink;
				}
				currentLink->nextLink = new LinkGraph<nameType, weightType>(_vectorOfName[row][column], _vectorOfWeight[row][column]);
			}
		}
	}
}
template<typename nameType, typename weightType>
Graph<nameType, weightType>::Graph(const Graph& _graph) {
	amountOfAllocatedMemory = _graph.amountOfAllocatedMemory;
	arrayOfLinks = new LinkGraph<nameType, weightType>*[amountOfAllocatedMemory];

	for (size_t i = 0; i < amountOfAllocatedMemory; ++i) {
		bool isFirstLink = true;
		LinkGraph<nameType, weightType>* link = _graph.arrayOfLinks[i];

		while (link != nullptr) {
			if (isFirstLink) {
				isFirstLink = false;
				arrayOfLinks[i] = new LinkGraph<nameType, weightType>(link->name, link->weight);
			}
			else {
				LinkGraph<nameType, weightType>* currentLink = arrayOfLinks[i];
				while (currentLink->nextLink != nullptr) {
					currentLink = currentLink->nextLink;
				}
				currentLink->nextLink = new LinkGraph<nameType, weightType>(link->name, link->weight);
			}
			link = link->nextLink;
		}
	}
}

template<typename nameType, typename weightType>
Graph<nameType, weightType>::~Graph() {
	for (size_t i = 0; i < amountOfAllocatedMemory; ++i) {
		if (arrayOfLinks[i]) {
			destroy(arrayOfLinks[i]);
		}
	}
	delete[] arrayOfLinks;
}

template<typename nameType, typename weightType>
size_t Graph<nameType, weightType>::size() const {
	return amountOfAllocatedMemory;
}
template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>** Graph<nameType, weightType>::data() {
	return arrayOfLinks;
}

template<typename nameType, typename weightType>
LinkGraph<nameType, weightType>* Graph<nameType, weightType>::getLinkGraph(const size_t _indexLink) const {
	return arrayOfLinks[_indexLink];
}

template<typename nameType, typename weightType>
Graph<nameType, weightType>& Graph<nameType, weightType>::operator= (const Graph& _rightGraph) {
	if (this != &_rightGraph) {
		amountOfAllocatedMemory = _rightGraph.amountOfAllocatedMemory;
		arrayOfLinks = new LinkGraph<nameType, weightType>*[amountOfAllocatedMemory];

		for (size_t i = 0; i < amountOfAllocatedMemory; ++i) {
			*arrayOfLinks[i] = *_rightGraph.arrayOfLinks[i];
		}
	}

	return *this;
}
