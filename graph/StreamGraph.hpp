#pragma once

#include <iostream>

template<typename nameType, typename weightType>
class Graph;

template<typename nameType, typename weightType>
std::ostream& operator << (std::ostream& stream, const Graph<nameType, weightType>& obj) {
	setlocale(LC_ALL, "RU");

	for (size_t i = 0; i < obj.amountOfAllocatedMemory; ++i) {
		LinkGraph<nameType, weightType>* linkGraph = obj.arrayOfLinks[i];

		stream << "����������� ������� " << i << " : " << std::endl;
		while (linkGraph != nullptr) {
			stream << "  ������� " << linkGraph->getName() << " ��� " << linkGraph->getWeight() << "." << std::endl;
			linkGraph = linkGraph->getNextLink();
		}
	}

	return stream;
}
