#pragma once

#include <vector>

using std::vector;

size_t fatherLink(const size_t amountOfElementHeap, const size_t link);

size_t firstChildLink(const size_t amountOfElementHeap, const size_t link);
size_t lastChildLink(const size_t amountOfElementHeap, const size_t link);

size_t smallestChildLink(const vector<int>& vectorOfKey, const size_t amountOfElementHeap, const size_t link);

void immersion(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap, const size_t link);
void surfacing(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap, const size_t link);

void withdrawalOfTheMinimum(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, size_t& amountOfElementHeap, int& keyMinimum, int& nameMinimum);

void queueUp(vector<int>& vectorOfName, vector<int>& vectorOfKey, vector<int>& vectorOfIndex, const size_t amountOfElementHeap);

