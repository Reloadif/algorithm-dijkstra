#include "Algorithm-Dijkstra.h"

const int infinity = 10000000;

void algorithmDijkstraOnDHeap(const Graph<int, int>& graph, vector<int>* dist, vector<int>* up, const size_t amountOfElementHeap, const size_t link) {
    dist->resize(amountOfElementHeap);
    up->resize(amountOfElementHeap);

    vector<int> vectorOfName(amountOfElementHeap);
    vector<int> vectorOfKey(amountOfElementHeap);
    vector<int> vectorOfIndex(amountOfElementHeap);

    for (size_t i = 0; i < amountOfElementHeap; ++i) {
        up->at(i) = 0;
        dist->at(i) = infinity;

        vectorOfName[i] = i;
        vectorOfKey[i] = infinity;
        vectorOfIndex[i] = i;
    }

    vectorOfKey[link] = 0;
    size_t amountOfElementQueue = amountOfElementHeap;
    queueUp(vectorOfName, vectorOfKey, vectorOfIndex, amountOfElementQueue);

    int nameMinimum, keyMinimum;
    while (amountOfElementQueue > 0) {
        withdrawalOfTheMinimum(vectorOfName, vectorOfKey, vectorOfIndex, amountOfElementQueue, keyMinimum, nameMinimum);
        int currentLink = nameMinimum;
        dist->at(currentLink) = keyMinimum;

        LinkGraph<int, int>* graphLink = graph.getLinkGraph(currentLink);
        int currentName, currentIndex;
        while (graphLink != 0) {
            currentName = graphLink->getName();
            currentIndex = vectorOfIndex[currentName];

            if (vectorOfKey[currentIndex] > (dist->at(currentLink) + graphLink->getWeight())) {
                vectorOfKey[currentIndex] = dist->at(currentLink) + graphLink->getWeight();
                surfacing(vectorOfName, vectorOfKey, vectorOfIndex, amountOfElementQueue, currentIndex);
                up->at(currentName) = currentLink;
            }
            graphLink = graphLink->getNextLink();
        }
    }

}
void algorithmDijkstraOnMark(const Graph<int, int>& graph, vector<int>* dist, vector<int>* up, const size_t link) {
    size_t numberOfVertex = graph.size();

    dist->resize(numberOfVertex);
    up->resize(numberOfVertex);

    vector<int> vectorOfMarks(numberOfVertex);

    for (size_t i = 0; i < numberOfVertex; ++i) {
        up->at(i) = 0;
        dist->at(i) = infinity;

        vectorOfMarks[i] = 0;
    }

    dist->at(link) = 0;
    size_t numberOfVertexQueue = numberOfVertex;

    while (numberOfVertexQueue > 0) {

        int c = 0;
        while (vectorOfMarks[c] != 0) {
            c = c + 1;
        }

        int i = c;
        for (int k = c + 1; k < numberOfVertex; ++k) {
            if (vectorOfMarks[k] == 0)
                if (dist->at(i) > dist->at(k))
                    i = k;
        }
        vectorOfMarks[i] = 1;
        numberOfVertexQueue = numberOfVertexQueue - 1;
        LinkGraph<int, int>* graphLink = graph.getLinkGraph(i);

        while (graphLink != 0) {
            int j = graphLink->getName();

            if (dist->at(j) > (dist->at(i) + graphLink->getWeight())) {
                dist->at(j) = dist->at(i) + graphLink->getWeight();
                up->at(j) = i;
            }
            graphLink = graphLink->getNextLink();

        }
    }
}