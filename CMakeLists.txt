cmake_minimum_required(VERSION 3.0)	 	# Проверка версии CMake.
										# Если версия установленой программы
set (CMAKE_CXX_STANDARD 17)							# старее указаной, произайдёт аварийный выход.

project(Shortest_Path_Problem)						# Название проекта

set(SOURCE_EXE ShortestPathProblem.cpp)					# Установка переменной со списком исходников для исполняемого файла

set(SOURCE_LIB_1 D-Heap.cpp)						# Тоже самое, но для библиотеки

set(SOURCE_LIB_2 Algorithm-Dijkstra.cpp)				# Тоже самое, но для библиотеки

add_library(D-Heap STATIC ${SOURCE_LIB_1})				# Создание статической библиотеки с именем D-Heap

add_library(Algorithm-Dijkstra STATIC ${SOURCE_LIB_2})			# Создание статической библиотеки с именем Algorithm-Dijkstra

add_executable(main ${SOURCE_EXE})					# Создает исполняемый файл с именем main

target_link_libraries(main D-Heap Algorithm-Dijkstra)			# Линковка программы с библиотекой

add_subdirectory(graph)
