#pragma once

#include <vector>

#include "graph/Graph.hpp"
#include "D-Heap.h"

void algorithmDijkstraOnDHeap(const Graph<int, int>& graph, vector<int>* dist, vector<int>* up, const size_t amountOfElementHeap, const size_t link);
void algorithmDijkstraOnMark(const Graph<int, int>& graph, vector<int>* dist, vector<int>* up, const size_t link);
